const test = require('tape')
const ssbKeys = require('ssb-keys')
const ssbURI = require('ssb-uri2')
const BFE = require('ssb-bfe')
const { DHKeys, directMessageKey } = require('ssb-private-group-keys')
const { keySchemes } = require('private-group-spec')

const Keyring = require('..')
const { tmpPath, formats } = require('./helpers')

test('keyring.dm', async t => {
  formats.forEach(format => {
    t.test(`> ${format} keys`, t => testKeys(t, format))
  })

  async function testKeys (t, format) {
    const myKeys = ssbKeys.generate(null, null, format)
    const theirKeys = ssbKeys.generate(null, null, format)
    const myId = myKeys.id
    const theirId = theirKeys.id
    const myIdURI = format === 'classic' ? ssbURI.fromFeedSigil(myId) : null
    console.log(myId)

    const path = tmpPath()
    let keyring = await Keyring(path)

    const myDhKeys = new DHKeys(undefined, { format: 0 }).generate()
    const theirDhKeys = new DHKeys(undefined, { format: 0 }).generate()

    const res = keyring.dm.add(myId, theirId, myDhKeys, theirDhKeys)
    t.equal(res, true, 'add() returns true because it was new')

    const res2 = keyring.dm.add(myId, theirId, myDhKeys, theirDhKeys)
    t.equal(res2, false, 'add() returns false because it was old')

    t.true(keyring.dm.has(myId, theirId), 'keyring.dm.has')
    if (myIdURI) t.true(keyring.dm.has(myIdURI, theirId), 'keyring.dm.has (URI)')

    const expected = directMessageKey(
      myDhKeys.toBFE().secret, myDhKeys.toBFE().public, BFE.encode(myId),
      theirDhKeys.toBFE().public, BFE.encode(theirId)
    )
    expected.scheme = keySchemes.feed_id_metafeed_dm

    t.deepEqual(keyring.dm.get(myId, theirId), expected, 'keyring.dm.get')
    t.notOk(keyring.dm.get(theirId, myId), 'keyring.dm.get')
    if (myIdURI) t.deepEqual(keyring.dm.get(myIdURI, theirId), expected, 'keyring.dm.get (URI)')

    await keyring.close()
    keyring = await Keyring(path)
    t.deepEqual(keyring.dm.get(myId, theirId), expected, '...persists')
    if (myIdURI) t.deepEqual(keyring.dm.get(myIdURI, theirId), expected, '...persists (URI)')

    await keyring.close()
    t.end()
  }
})

// write test with add, no await + close
