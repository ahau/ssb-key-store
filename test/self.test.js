/* eslint-disable camelcase */

const test = require('tape')
const { keySchemes } = require('private-group-spec')
const { SecretKey } = require('ssb-private-group-keys')
const na = require('sodium-universal')

const Keyring = require('../')
const { tmpPath } = require('./helpers')

test('keyring.self', async t => {
  const path = tmpPath()
  let keyring = await Keyring(path)

  /* init ensures a self key is there */
  const ownKeys = keyring.self.get()
  t.equal(ownKeys.scheme, keySchemes.feed_id_self, 'self.get => ownKeys (scheme)')
  t.true(Buffer.isBuffer(ownKeys.key), 'self.get => ownKeys (key)')

  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.self.get(), ownKeys, '...persists')

  /* self.set */
  const newKey = new SecretKey().toBuffer()
  t.equal(newKey.length, na.crypto_secretbox_KEYBYTES, 'newKey must be na.crypto_secretbox_KEYBYTES long')
  await keyring.self.set({ key: newKey })
  const expected = { key: newKey, scheme: keySchemes.feed_id_self }
  t.deepEqual(keyring.self.get(), expected, 'self.set')

  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.self.get(), expected, '...persists')

  keyring.close()
  t.end()
})
