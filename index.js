/* eslint-disable brace-style */

const { promisify } = require('util')

const db = require('./db')

const Self = require('./models/self')
const DM = require('./models/dm')
const Group = require('./models/group')
const POBox = require('./models/po-box')
const Signing = require('./models/signing')

module.exports = function Keyring (path, cb) {
  if (cb === undefined) return promisify(Keyring)(path)
  const level = db(path)

  const self = Self(level)
  const dm = DM(level)
  const group = Group(level)
  const poBox = POBox(level, dm)
  const signing = Signing(level)

  const api = {
    self: {
      set: self.set,
      get: self.get
    },
    signing: {
      add: signing.add,
      addNamed: signing.addNamed,
      get: signing.get,
      has: signing.has
    },
    dm: {
      add: dm.add,
      addTriangle: dm.addTriangle,
      has: dm.has,
      get: dm.get,
      triangulate: dm.triangulate,
      list: dm.list
    },
    group: {
      add: group.add,
      pickWriteKey: group.pickWriteKey,
      has: group.has,
      get: group.get,
      getUpdates: group.getUpdates,
      exclude: group.exclude,
      list: group.list,
      listSync: group.listSync
    },
    poBox: {
      add: poBox.add,
      has: poBox.has,
      get: poBox.get,
      list: poBox.list
    },
    close: level.close.bind(level)
  }

  /* loads persisted states into cache */
  group.load(err => {
    if (err) return cb(err)
    self.load(err => {
      if (err) return cb(err)
      dm.load(err => {
        if (err) return cb(err)
        signing.load(err => {
          if (err) return cb(err)
          poBox.load(err => {
            if (err) return cb(err)
            cb(null, api)
          })
        })
      })
    })
  })
  // TODO
  // - parallelize
  // - close level if there was an error!
}
